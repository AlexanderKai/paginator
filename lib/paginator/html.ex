defmodule Paginator.HTML do
  use Phoenix.HTML
  use Phoenix.Controller

  def get_pagination_link(conn, opts) do
    type = Keyword.get(opts, :type)
    text = Keyword.get(opts, :text, "Get entries")
    cursor = 
      case type do
        :after -> conn.assigns.pagination[:cursor_after]
        :before -> conn.assigns.pagination[:cursor_before]
      end
    url = conn.query_params 
          |> Map.drop(["after", "before"]) 
          |> Map.put(type, cursor) 
    case cursor do
      nil -> ""
      _ -> link(text, to: current_path(conn, url))
    end
  end
end
